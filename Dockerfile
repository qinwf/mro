FROM ubuntu:16.04

RUN useradd docker \
	&& mkdir /home/docker \
	&& chown docker:docker /home/docker \
	&& addgroup docker staff

RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
	&& locale-gen en_US.utf8 \
	&& /usr/sbin/update-locale LANG=en_US.UTF-8

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8

## Install some useful tools and dependencies for MRO

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
	ca-certificates \
	curl build-essential \
	&& rm -rf /var/lib/apt/lists/*

# Use major and minor vars to re-use them in non-interactive installtion script
ENV MRO_VERSION_MAJOR 3
ENV MRO_VERSION_MINOR 3
ENV MRO_VERSION_BUGFIX 1
ENV MRO_VERSION $MRO_VERSION_MAJOR.$MRO_VERSION_MINOR.$MRO_VERSION_BUGFIX

WORKDIR /home/docker

# Download, valiate, and unpack
RUN curl -LO -# https://mran.microsoft.com/install/mro/$MRO_VERSION/microsoft-r-open-$MRO_VERSION.tar.gz
RUN tar -xvf microsoft-r-open-$MRO_VERSION.tar.gz

# Install MRO, which inkludes MKL, see https://mran.microsoft.com/documents/rro/installation/
WORKDIR /home/docker/microsoft-r-open
RUN ./install.sh -a -u \
	&& ls logs && cat logs/*

# Print MKL and MRO EULAs on every start
ENV PROFILE /usr/lib64/microsoft-r/$MRO_VERSION_MAJOR.$MRO_VERSION_MINOR/lib64/R/etc/Rprofile.site

# Clean up
WORKDIR /home/docker
RUN rm microsoft-r-open-$MRO_VERSION.tar.gz \
	&& rm -r microsoft-r-open



CMD ["/usr/bin/R"]
